# Setup
Requires local mvn install of sendgrid 4.8.2: https://github.com/sendgrid/sendgrid-java

```bash
git clone https://github.com/sendgrid/sendgrid-java.git
cd sendgrid-java
mvn install -DskipTests
```

# Configuration

Configuration done via ENV variables

## RAKENINTERVIEW_EMAILSERVICE_FILTER
Behavior: If present enables filtering to just send to `rakenapp.com`, other addresses dropped as if not provided

Values: any

## RAKENINTERVIEW_EMAILSERVICE_LIVE
Behavior: If present enables actual live sending of emails, otherwise just processes and logs as if the email were sent

Values: any

Requires: `RAKENINTERVIEW_EMAILSERVICE_API_KEY` 

## RAKENINTERVIEW_EMAILSERVICE_API_KEY
Behavior: Assigns the API key used for sendgrid

Values: string

# Future (Developer prioritized)
For real production logic continued work to be considered

Feature Completion

* Properly json-ify response body of final email sent
* Clean up cat-fact enrichment from raw json to just the fact string
* Create Swagger/alternate API auto doc bindings
* Expose Service sender address configuration

Security & Scale

* Rate limiting backoff responses (propose bucket limiting to 60 requests per second)
* Use application config files for secret api key instead of env variables to enable OS FS security protections
* Load tests to find point of failure under stress
* Optimization pass to reasonably extend point of failure
* Hook logging to Splunk/alternate log collection methodology

Maintainability

* Refactor EmailController::send to an interface to fully isolate sendgrid dependency
* Automate sendgrid dependency build step
* Lock sendgrid dependency to local mirror and not public latest
* Switch to a proper logger instead of System.out/err
* Create live test suite that can test send logic automatically (testing done by hand currently)
* Containerize deployment
* Switch EmailPostObject to empty list default for easier maintenance
* Generalize implementation of address filter to use white/blacklist
** Can still keep current config flags that just setup default white/blacklist
* Configure custom runtime exception for controller to throw response-entity convertible exceptions to allow helper methods that can force responses without return marshaling

Feature Enhancements

* Expose enrichment interface and random sample, expose enrichment types via config
* Send to multiple singularly parameter