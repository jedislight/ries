package com.rakeninterview.emailservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmailServiceApplicationTests {

	@Autowired
	private EmailController emailController;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate rest;


	private String host() {
		return "http://localhost:" + port;
	}

	@BeforeEach
	void beforeEach() {
		emailController.setRakenappFilterEnabled(false);
	}

	@Test
	void contextLoads() {
		assertNotNull(emailController);
	}

	@Test
	void postHelloWorld() throws JSONException {
		assertEquals(HttpStatus.OK, postEmailRequest(getDefaultPostBody()).getStatusCode());
	}

	@Test
	void postNoToLineBadRequest() throws  JSONException {
		JSONObject body = getDefaultPostBody();
		body.put("to", new JSONArray(Collections.EMPTY_LIST));
		assertEquals(HttpStatus.BAD_REQUEST,postEmailRequest(body).getStatusCode());
	}

	@Test
	void postNoTargetsBadRequest() throws  JSONException {
		JSONObject body = getDefaultPostBody();
		body.put("to", new JSONArray(Collections.EMPTY_LIST));
		body.put("cc", new JSONArray(Collections.EMPTY_LIST));
		body.put("bcc", new JSONArray(Collections.EMPTY_LIST));
		assertEquals(HttpStatus.BAD_REQUEST,postEmailRequest(body).getStatusCode());
	}

	@Test
	void postFilteredTargetBadRequest() throws  JSONException {
		emailController.setRakenappFilterEnabled(true);

		JSONObject body = getDefaultPostBody();
		body.put("to", new JSONArray(List.of("filterMe@example.com")));
		assertEquals(HttpStatus.BAD_REQUEST,postEmailRequest(body).getStatusCode());
	}

	@Test
	void postUnfilteredTargetOk() throws  JSONException {
		emailController.setRakenappFilterEnabled(true);

		JSONObject body = getDefaultPostBody();
		body.put("to", new JSONArray(List.of("dontfilterme@rakenapp.com")));
		assertEquals(HttpStatus.OK,postEmailRequest(body).getStatusCode());
	}

	@Test
	void postUnfilteredMixedCaseTargetOk() throws  JSONException {
		emailController.setRakenappFilterEnabled(true);

		JSONObject body = getDefaultPostBody();
		body.put("to", new JSONArray(List.of("dontfilterme@RaKeNaPp.cOm")));
		assertEquals(HttpStatus.OK,postEmailRequest(body).getStatusCode());
	}

	@Test
	void postEnrichmentContainsEnrichmentContent() throws JSONException {
		JSONObject body = getDefaultPostBody();
		assertFalse(body.getString("body").contains("Powered by"));
		ResponseEntity<String> response = postEmailRequest(body, true);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.hasBody());
		assertTrue(response.getBody().contains("Powered by"));
	}

	@Test
	void postNonEnrichmentContainsNoEnrichmentContent() throws JSONException {
		JSONObject body = getDefaultPostBody();
		assertFalse(body.getString("body").contains("Powered by"));
		ResponseEntity<String> response = postEmailRequest(body, false);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.hasBody());
		assertFalse(response.getBody().contains("Powered by"));
	}


	private ResponseEntity<String> postEmailRequest(JSONObject body, boolean enrich) {
		return postEmailRequest(makeJsonRequest(body), enrich);
	}

	private ResponseEntity<String> postEmailRequest(JSONObject body) {
		return postEmailRequest(body, false);
	}

	private ResponseEntity<String> postEmailRequest(HttpEntity<String> request) {
		return postEmailRequest(request, false);
	}
	private ResponseEntity<String> postEmailRequest(HttpEntity<String> request, boolean enrich) {
		String url = host() + "/email/";
		if (enrich) {
			url += "?enrich=1";
		}
		return rest.postForEntity(url, request, String.class);
	}

	private HttpEntity<String> makeJsonRequest(JSONObject jsonObject) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request =
				new HttpEntity<String>(jsonObject.toString(), headers);
		return request;
	}

	private JSONObject getDefaultPostBody() throws JSONException {
		JSONObject emailPostBody = new JSONObject();
		emailPostBody.put("subject", "Subject");
		emailPostBody.put("body", "Body");
		emailPostBody.put("to", new JSONArray(List.of("example@example.com")));
		emailPostBody.put("cc", new JSONArray(Collections.EMPTY_LIST));
		emailPostBody.put("bcc", new JSONArray(Collections.EMPTY_LIST));
		return emailPostBody;
	}
}
