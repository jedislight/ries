package com.rakeninterview.emailservice;

import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.sendgrid.*;

/*
* Email Controller for sending emails via POST requests
* Sender is no-reply@raken-interview-email-service.com
*/
@RestController
@RequestMapping(path = "/email")
public class EmailController {

    // FUTURE - expose via configuration file
    private boolean rakenappFilterEnabled = System.getenv().containsKey("RAKENINTERVIEW_EMAILSERVICE_FILTER");
    private boolean liveSendingEnabled = System.getenv().containsKey("RAKENINTERVIEW_EMAILSERVICE_LIVE");
    private String sendApiKey = System.getenv().getOrDefault("RAKENINTERVIEW_EMAILSERVICE_API_KEY", null);

    // Exposed for testing overrides
    void setRakenappFilterEnabled(boolean rakenappFilterEnabled) {
        this.rakenappFilterEnabled = rakenappFilterEnabled;
    }

    // Exposed for testing overrides
    void setLiveSendingEnabled(boolean liveSendingEnabled) {
        this.liveSendingEnabled = liveSendingEnabled;
    }

    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> postEmail(
            @RequestBody EmailPostObject emailPostObject,
            @RequestParam(required = false) boolean enrich) {
        System.out.println("Request in: " + emailPostObject);

        // filter out addresses if enabled
        if (rakenappFilterEnabled) {
            emailPostObject.setTo(filterNonRakenAppEmails(emailPostObject.getTo()));
            emailPostObject.setCc(filterNonRakenAppEmails(emailPostObject.getCc()));
            emailPostObject.setBcc(filterNonRakenAppEmails(emailPostObject.getBcc()));
        }

        // ensure any addresses are present
        if (emailPostObject.getAddressCount() == 0) {
            return new ResponseEntity<>("{\"error\" : \"At least on valid email address required\"}", HttpStatus.BAD_REQUEST);
        }

        // ensure TO address present
        // NOTE constraint for TO comes from sendgrid, all BCC/CC mailing is valid on many platforms
        if (emailPostObject.getTo().size() == 0) {
            return new ResponseEntity<>("{\"error\" : \"At least on valid TO address required\"}", HttpStatus.BAD_REQUEST);
        }

        // if requested enrich email with cute footer
        if (enrich) {
            String enrichmentString = "Powered by Unicorns and Magic\n";
            try {
                enrichmentString += getDynamicEnrichmentContent();
            } catch(Exception e) {
                System.err.println("WARNING - Failed to get dynamic enrichment content");
                e.printStackTrace(System.err);
            }

            emailPostObject.setBody(emailPostObject.getBody()+"\n"+enrichmentString);
        }

        // if enabled, send the actual email now
        if (liveSendingEnabled) {
            if (sendApiKey == null || sendApiKey.isEmpty()) {
                // do NOT send internal failure details to client
                System.err.println("ERROR - API key not set");
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ResponseEntity<Object> sendResponse = null;
            try {
                 sendResponse = send(emailPostObject);
            } catch (IOException e) {
                e.printStackTrace(System.err);
                // do NOT send internal failure details to client
                System.err.println("Failed to send email " + emailPostObject);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            if (sendResponse == null || !sendResponse.getStatusCode().is2xxSuccessful()) {
                System.err.println("Remote service failure " + sendResponse.getStatusCode() + " " + sendResponse.getBody());
                // do NOT send internal failure details to client
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        // success, send back the final email as sent with filter + enrichment etc
        System.out.println("Final request as sent " + emailPostObject);
        return new ResponseEntity<>(emailPostObject.toString(), HttpStatus.OK);
    }

    // Reference: https://docs.sendgrid.com/for-developers/sending-email/v3-java-code-example
    private ResponseEntity<Object> send(EmailPostObject emailPostObject) throws IOException {
        assert emailPostObject.getTo() != null;
        assert emailPostObject.getTo().isEmpty() == false;

        Email from = new Email("no-reply@raken-interview-email-service.com");
        String subject = emailPostObject.getSubject();
        Content content = new Content("text/html", emailPostObject.getBody());
        Mail mail = new Mail(from, subject, new Email(emailPostObject.getTo().get(0)), content);
        Personalization personalization = new Personalization();

        if (emailPostObject.getBcc() != null) {
            emailPostObject.getBcc().stream()
                    .map(Email::new)
                    .forEach(personalization::addBcc);
        }

        if (emailPostObject.getCc() != null) {
            emailPostObject.getCc().stream()
                    .map(Email::new)
                    .forEach(personalization::addCc);
        }

        // NOTE - sendgrid API reqires personalization to contain full TO info
        // even if TO already provided in base Mail object construction
        emailPostObject.getTo().stream()
                .map(Email::new)
                .forEach(personalization::addTo);

        mail.addPersonalization(personalization);

        SendGrid sg = new SendGrid(sendApiKey);
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        Response response = sg.api(request);
        return new ResponseEntity<>(response.getBody(), HttpStatus.valueOf(response.getStatusCode()));
    }

    private String getDynamicEnrichmentContent() throws IOException {
        URL url = new URL("https://catfact.ninja/fact");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        String content = new BufferedReader(new InputStreamReader(connection.getInputStream()))
                .lines()
                .collect(Collectors.joining("\n"));
        connection.disconnect();

        return content;
    }

    private List<String> filterNonRakenAppEmails(List<String> list) {
        return list.stream()
            .filter(address->address.toLowerCase(Locale.ROOT).endsWith("rakenapp.com"))
            .collect(Collectors.toList());
    }
}
