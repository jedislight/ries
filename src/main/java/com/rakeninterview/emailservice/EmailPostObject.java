package com.rakeninterview.emailservice;


import java.util.List;

public class EmailPostObject {
    private String subject;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String body;

    public EmailPostObject (String subject, List<String> to, List<String> cc, List<String> bcc, String body ) {
        this.bcc = bcc;
        this.to = to;
        this.cc = cc;
        this.subject = subject;
        this.body = body;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public List<String> getCc() {
        return cc;
    }

    public List<String> getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        // FUTURE JSON-ify
        return "EmailPostObject{" +
                "subject='" + subject + '\'' +
                ", to=" + to +
                ", cc=" + cc +
                ", bcc=" + bcc +
                ", body='" + body + '\'' +
                '}';
    }

    public int getAddressCount() {
        return
            (to != null ? to.size() : 0) +
            (bcc != null ? bcc.size() : 0) +
            (cc != null ? cc.size() : 0);
    }
}